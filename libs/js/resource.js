(function(ang) {
	
	var module = ang.module('resource',[]);

	/*
		var asd = new Resource('/asd/:asdId');
		asd.post({
			data : {
				asd : 123
			},
			urlParams : {
				asdId : 123
			}
		}).then(function(resp) {
			console.log(resp)
		})
	*/

	module.factory('Resource', ['$http', function($http) {
		return function(url) {
			// Variables
				var availableMethods = ['GET', 'POST', 'PUT', 'DELETE'],
					methods = {},
					methodsCallbacks = {};

			// Methods
				var processUrl = function(url, params) {
					if (!params) params = {};
					var urlParams = (url.match(/\:([a-zA-Z])+/g) || []);
					urlParams.forEach(function(param) {
						param = param.substring(1,param.length);
						if (!params[param]) {
							url = url.replace(':'+param+'/', '');
							url = url.replace(':'+param, '');
							return;
						}
						// url = url.replace(':'+param+'/', params[param]);
						url = url.replace(':'+param, params[param]);
					});

					return url;
				}

			// Init
				// TODO DANIEL
				availableMethods.forEach(function(method) {
					methods[method.toLowerCase()] = function(params) {
						$http({
							method : method,
							url : processUrl(url, !params || params.urlParams),
							data : (!params || params.data)
						}).success(function(response) {
							if(methodsCallbacks[method]){
								methodsCallbacks[method](response);
							}
						});
						return {
							then : function(callback) {
								methodsCallbacks[method] = callback;
							}
						}
					}
				});
			return methods;
		}
	}]);

})(angular)