(function(ang) {

	var app = ang.module('app', [
		'ui.router',
		'uiGmapgoogle-maps',
		'sit-tour'
	]);

})(angular)