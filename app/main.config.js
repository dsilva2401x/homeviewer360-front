(function(ang) {

	var app = ang.module('app');

	app.config(function($urlRouterProvider, $stateProvider, uiGmapGoogleMapApiProvider) {

		uiGmapGoogleMapApiProvider.configure({
			// key: 'your api key',
			v: '3.17',
			libraries: 'weather,geometry,visualization'
		});

	});

})(angular)