(function(ang) {

	var app = ang.module('sit-tour');

	/**
		@ngdoc service
		@name sit-tour.factory:downloadViewData
		@requires $http
		@requires $filter
		@requires Resources
		@description
		Downloads and filter view data and returns it in a callback
		@param {integer} viewId View id
		@param {function} callback to get data
	*/
	app.factory('downloadViewData', function($http, $filter, Resources) {
		return function(viewId, callback) {
			console.log('Downloading data for view-'+viewId+'..');
			Resources.View.get({
				urlParams: {
					viewId: viewId
				}
			}).then(function(data) {
				data = $filter('viewData')(data);
				// Load images
				// TODO
				data.adjViews.forEach(function(v) {
					v.img = new Image;
					v.img.src = v.imgSrc;
				})
				console.log( data );
				callback(data);
			});
		};
	});

	/**
		@ngdoc service
		@name sit-tour.factory:EquiTour
		@requires downloadViewData
		@description
		Provides a function to instance Equitour
		@param {object} config Equitour configuration
	*/	
	app.factory('EquiTour', function(downloadViewData) {
		return function(config) {

			// Variables
				var container, equiviewrFront, equiviewrBack, 
				self, containerBack, containerFront,
				self = this;

			// Methods
				var loadMarkers = function (markers) {
					markers.forEach(function (m) {
						equiviewrFront.addMarker(m.alpha, m.beta, m.link);
					});
				}

				var init = function() {
					container = config.container;
					containerBack = document.createElement('div');
					containerFront = document.createElement('div');
					containerBack.style['position'] = 'absolute';
					containerBack.style['left'] = '0px';
					containerBack.style['top'] = '0px';
					containerBack.style['width'] = '100%';
					containerBack.style['height'] = '100%';
					containerFront.style['position'] = 'absolute';
					containerFront.style['left'] = '0px';
					containerFront.style['top'] = '0px';
					containerFront.style['width'] = '100%';
					containerFront.style['height'] = '100%';
					containerFront.style['-webkit-transition-duration'] = '0.5s';
					containerFront.style['-moz-transition-duration'] = '0.5s';
					containerFront.style['-o-transition-duration'] = '0.5s';
					containerFront.style['-ms-transition-duration'] = '0.5s';
					container.appendChild(containerBack);
					container.appendChild(containerFront);
					equiviewrFront = new EquiViewr({
						container: containerFront,
						limits: {
							minY: -30
						}
					});
					equiviewrBack = new EquiViewr({
						container: containerBack,
						limits: {
							minY: -30
						}
					});
					self._equiviewrFront = equiviewrFront;
					self._equiviewrBack = equiviewrBack;
				}

				var changeView = function(newView) {
					// @1 Change back image and set it to zoom and position of new view
					// @3 Start soft transition to back image
					// @4 Update front image
					// @5 Zoom soft reset in front image

					containerFront.style['-webkit-transition-duration'] = '0.5s';
					containerFront.style['-moz-transition-duration'] = '0.5s';
					containerFront.style['-o-transition-duration'] = '0.5s';
					containerFront.style['-ms-transition-duration'] = '0.5s';

					equiviewrBack.updateImage(newView.imgSrc);
					equiviewrBack.moveTo({
						x: (newView.xDirToCurrent+180)%360,
						// x: newView.xDirToCurrent,
						y: 0
					});
					equiviewrFront.zoomIn(function() {
						containerFront.style['opacity'] = 0;
						setTimeout(function() {
							equiviewrFront.updateImage(newView.imgSrc);
							equiviewrFront.moveTo({
								x: (newView.xDirToCurrent+180)%360,
								// x: newView.xDirToCurrent,
								y: 0
							});
							equiviewrFront.resetZoom();
							setTimeout(function() {
								containerFront.style['-webkit-transition-duration'] = '0.5s';
								containerFront.style['-moz-transition-duration'] = '0.5s';
								containerFront.style['-o-transition-duration'] = '0.5s';
								containerFront.style['-ms-transition-duration'] = '0.5s';
								containerFront.style['opacity'] = 1;
							},200);
							// Add targets
							newView.adjViews.forEach(function(v) {
								equiviewrFront.addTarget(v.xDir.fromCurrent, {
									title: v.title,
									action: function() {
										// Download
										downloadViewData(v.id, function(data) {
											data.xDirToCurrent = v.xDir.fromTarget;
											changeView(data);
										});
									}
								})
							})
							// Add markers
							// equiviewrFront.addMarker(0,12);
							loadMarkers( newView.markers );
						},700);
					});
				}

				/**
					@ngdoc method
					@name initView
					@methodOf sit-tour.factory:EquiTour
					@param {object} data View data
				**/

				this.initView = function(data) {
					// Update view
					equiviewrFront.updateImage(data.imgSrc);
					data.adjViews.forEach(function(view) {
						equiviewrFront.addTarget(view.xDir.fromCurrent, {
							title: view.title,
							action: function() {
								// Download
								downloadViewData(view.id, function(data) {
									data.xDirToCurrent = view.xDir.fromTarget;
									changeView(data);
								});
							}
						})
					});

					// equiviewrFront.addMarker(0,12);
					loadMarkers( data.markers );
				}

			// Construct
				init();

		};
	});


})(angular)