(function(ang) {

	var app = ang.module('sit-tour');

	/**
		@ngdoc filter
		@name sit-tour.filter:viewData
		@description Filter view Data
	*/
	app.filter('viewData', function() {
		return function(data) {
			return data; 
			var fData = {};
			fData.id = data.id;
			fData.title = data.nombre;
			fData.imgSrc = data.imagen;
			fData.adjViews = [];
			for(var i=0;i<data.conexiones_origen.length;i++){
				fData.adjViews.push({
					id: data.conexiones_origen[i].vista_destino.id,
					title: data.conexiones_origen[i].vista_destino.nombre,
					imgSrc: data.conexiones_origen[i].vista_destino.imagen,
					xDir: {
						fromCurrent: data.conexiones_origen[i].x_origen,
						fromTarget: data.conexiones_origen[i].x_destino
					}
				})
			}
			return fData;
		}
	});

	/**
		@ngdoc filter
		@name sit-tour.filter:buildingData
		@description Filter building Data
	*/
	app.filter('buildingData', function() {
		return function(data) {
			return data;
			var fData = {};
			fData.id = data.id;
			fData.location = {
				latitude: data.latitud,
				longitude: data.longitud
			};
			fData.views = [];
			data.vistas.forEach(function (v) {
				// TODO : DELETE
				v.imagen = v.imagen.replace('testserver', '70.32.78.218:8008');
				fData.views.push({
					id: v.id,
					name: v.nombre,
					imgSrc: v.imagen
				});
			});
			return fData;
		}
	});

})(angular)