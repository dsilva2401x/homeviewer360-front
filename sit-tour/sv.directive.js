(function(ang) {

	var module = ang.module('sit-tour');

	module.directive('sv', function(EquiTour) {
		return {
			restrict: 'EA',
			link: function( scope, elem, attrs ) {
				var mapOptions = {
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(elem[0],mapOptions);
				var myPos = new google.maps.LatLng(
					scope.models.currentBuilding.location.latitude,
					scope.models.currentBuilding.location.longitude
				);
				map.setCenter(myPos);
				var fenway = new google.maps.LatLng(
					scope.models.currentBuilding.location.latitude,
					scope.models.currentBuilding.location.longitude
				);
				var panoramaOptions = {
				position: fenway,
					pov: {
						heading: 34,
						pitch: 10
					}
				};
				var panorama = new google.maps.StreetViewPanorama(
					elem[0],
					panoramaOptions
				);
				map.setStreetView(panorama);
			}
		}
	});

})(angular)