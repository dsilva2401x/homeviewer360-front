(function(ang) {

	/**
		@ngdoc object
		@name sit-tour
		@requires resource
		@description
		Main module for sit-tour
	*/

	var module = ang.module('sit-tour', ['resource']);

})(angular)