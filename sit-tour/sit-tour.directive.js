(function(ang) {

	var module = ang.module('sit-tour');

	/**
		@ngdoc directive
		@name sit-tour.directive:sitTour
		@restrict EA
		@description
		Directive used to set tour view
	*/
	module.directive('sitTour', [function() {
		return {
			restrict: 'EA',
			templateUrl: 'sit-tour/sit-tour.html',
			controller: 'sitTour',
			scope: {},
			replace: true
		};
	}]);

})(angular)