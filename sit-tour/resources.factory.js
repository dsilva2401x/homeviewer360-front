/**
	[1]
	@ngdoc service
	@name sit-tour.factory:Resources
	@requires $http
	@requires Resource

	[2]
	@ngdoc object
	@name View
	@propertyOf sit-tour.factory:Resources
	@description Provides the route to get Views data using Resource service

	[3]
	@ngdoc object
	@name Building
	@propertyOf sit-tour.factory:Resources
	@description Provides the route to get Buildings data using Resource service
*/

(function(ang) {

	var app = ang.module('sit-tour');

	// [1]
	app.factory('Resources', function($http, Resource) {
		var Resources = {};
		var domain = 'http://70.32.78.218:8008';
		// [2]
		Resources.View = new Resource('demos/demo:viewId.json');
		// Resources.View = new Resource(domain+'/api/v1/vistas/:viewId/');
		// [3]
		Resources.Building = new Resource('demos/buildingDemo.json');
		// Resources.Building = new Resource(domain+'/api/v1/inmuebles/:buildingId/');

		return Resources;
	});

})(angular)