/**
	[1]
	@ngdoc controller
	@name sit-tour.controller:sitTour
	@requires $scope
	@requires $filter
	@requires Resources
	@requires downloadViewData
	@description
	sit-tour module controller

	[2]
	@ngdoc method
	@name sit-tour.controller:loadBuilding
	@methodOf sit-tour.controller:sitTour
	@description
	Loads building data
	@param {integer} buildingId Building id

	[3]
	@ngdoc method
	@name sit-tour.controller:changeView
	@methodOf sit-tour.controller:sitTour
	@description
	Change current equitour view
	@param {integer} viewId View id

	[4]
	@ngdoc property
	@name viewsContainerStatus
	@propertyOf sit-tour.controller:sitTour
	@description Saves the view container status

	[5]
	@ngdoc property
	@name currentView
	@propertyOf sit-tour.controller:sitTour
	@description Saves the current view status
**/
(function(ang) {

	var module = ang.module('sit-tour');

	// [1]
	module.controller('sitTour', ['$scope', '$filter', 'Resources', 'downloadViewData', function($scope, $filter, Resources, downloadViewData) {
		if(!$scope.models) $scope.models = {};
		if(!$scope.methods) $scope.methods = {};

		// Methods
			// [2]
			$scope.methods.loadBuilding = function (buildingId) {
				Resources.Building.get({
					urlParams: {
						buildingId: buildingId
					}
				}).then(function(buildingData) {
					buildingData = $filter('buildingData')(buildingData);
					$scope.models.currentBuilding = buildingData;
					downloadViewData( buildingData.views[0].id, function(viewData) {
						$scope.models.currentViewId = buildingData.views[0].id;
						$scope.models.equiTour.initView( viewData );
					});
				});
			}
			// [3]
			$scope.methods.changeView = function (viewId) {
				downloadViewData( viewId, function(viewData) {
					$scope.models.currentViewId = viewId;
					$scope.models.equiTour.initView( viewData );
					$scope.models.equiTour._equiviewrFront.moveTo({
						x: 180,
						y: 0
					});
				});
			}
			// 
			$scope.methods.scrollSides = function (leftOrRight) {
				// TODO: Change angular style dom manipulation
				var container = document.getElementById('vwsC');
				var subContainer = document.getElementById('vwsSC');
				var vItems = document.getElementsByClassName('view-item');
				var cW = container.offsetWidth;
				var scW = subContainer.offsetWidth;
				var step = 0;
				var scL = subContainer.offsetLeft;
				if ( scW <= cW || !vItems.length ) return;
				if ( scL >= 0 && leftOrRight==-1 ) return;
				if ( (scW+scL) <= cW && leftOrRight==1 ) return;
				step = (vItems[0].offsetWidth+8)*leftOrRight*-1;
				subContainer.style.left = scL+step+'px';
			}

		// Models
			// [4]
			$scope.models.viewsContainerStatus = true;
			// [5]
			$scope.models.currentView = 'views';

		// Init
			// Get building id
			var _urlParams = window.location.search;
			var urlParams = {};
			_urlParams = _urlParams.substring(1,_urlParams.length);
			_urlParams = _urlParams.split('&');
			_urlParams.forEach(function (up) {
				up = up.split('=');
				urlParams[ up[0] ] = up[1];
			});
			if (!urlParams.bId) {
				document.body.innerHTML = 'Ingrese el id del inmueble<br>Ej:<br>url..?bId=123';
				return;
			}
			$scope.methods.loadBuilding( urlParams.bId );

	}]);

})(angular)