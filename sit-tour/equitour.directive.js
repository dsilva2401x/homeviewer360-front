(function(ang) {

	var module = ang.module('sit-tour');

	/**
		@ngdoc directive
		@name sit-tour.directive:equiviewr
		@restrict EA
		@requires EquiTour
		@description
		Used to set equirectangular views
	*/

	module.directive('equiviewr', function(EquiTour) {
		return {
			restrict: 'EA',
			link: function( scope, elem, attrs ) {
				if (!scope.models) scope.models = {};

				/**
					@ngdoc property
					@name equiTour
					@propertyOf sit-tour.directive:equiviewr
					@description Instance an equitour object
				*/
				scope.models.equiTour = new EquiTour({
					container: elem[0]
				});

			}
		}
	});

})(angular)