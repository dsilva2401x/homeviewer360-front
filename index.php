<html lang="es" ng-app="app">

	<head>
		<link rel="stylesheet" href="app/main.css">
		<link rel="stylesheet" href="sit-tour/sit-tour.css">
	</head>

	<body>

		<sit-tour
			style="
				position: absolute;
				left: 50px;
				top: 50px;
				width: calc( 100% - 100px );
				height: calc( 100% - 100px );
			"
		></sit-tour>

		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script src="libs/js/three.min.js"></script>
		<script src="libs/js/threex.min.js"></script>
		<script src="libs/js/lodash.min.js"></script>
		<script src="libs/js/angular.js"></script>
		<script src="libs/js/angular-ui-router.min.js"></script>
		<script src="libs/js/angular-google-maps.min.js"></script>
		<script src="libs/js/equiviewr.js"></script>
		<script src="libs/js/resource.js"></script>
		<script src="app/main.js"></script>
		<script src="app/main.config.js"></script>
		<script src="sit-tour/sit-tour.js"></script>
		<script src="sit-tour/resources.factory.js"></script>
		<script src="sit-tour/sit-tour.controller.js"></script>
		<script src="sit-tour/sit-tour.directive.js"></script>
		<script src="sit-tour/equitour.directive.js"></script>
		<script src="sit-tour/equitour.factories.js"></script>
		<script src="sit-tour/equitour.filters.js"></script>
		<script src="sit-tour/gmaps.directive.js"></script>
		<script src="sit-tour/sv.directive.js"></script>
	</body>

</html>